import { shoppingCart } from './shoppingCart.js';
import cloneDeep from './node_modules/lodash-es/cloneDeep.js';

;

// shoppingCart.addToCart("shampoos", 10);
// shoppingCart.addToCart("conditionrs", 23);
// console.log(shoppingCart.price);
// console.log(shoppingCart.cart.filter(product => product.quantity > 10));

const state = {
    cart: [
        {
            name: "shampoo",
            price: 1200
        },
        {
            name: "cream",
            price: 1000
        }
    ],
    user: {
        loggedIn: true
    }

};

const stateClone = Object.assign({}, state);
// state.user.loggedIn = false;
console.log(stateClone);

const stateDeepClone = cloneDeep(state);
state.user.loggedIn = false;
console.log(stateDeepClone);
