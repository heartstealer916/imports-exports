
const log = console.log;
// let cart = [];
// export const addToCart = function (product, quantity)
// {
//     cart.push({ product, quantity });
//     log(`${quantity} ${product}  are added to cart`);
//     log(cart);

// };

export const shoppingCart = (function ()
{
    const cart = [];
    const products = 100;
    const price = 100;
    const addToCart = function (product, quantity)
    {
        cart.push({ product, quantity });
        log(`${quantity} ${product}  are added to cart within ${products}`);
        // log(cart);

    };
    return {
        cart,
        price,
        addToCart
    };
})();
